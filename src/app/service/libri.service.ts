import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Libro } from '../libro';

@Injectable({
  providedIn: 'root'
})
export class LibriService {

  url:string="http://liag.dynu.net:8080/api/libri";//scriviamo sul server del prof. have fun
  constructor(public http:HttpClient) {
   }

   getAll():Observable<Libro[]>{//restituirà un Observable
    return this.http.get<Libro[]>(this.url);
   }

   //funzione per aggiungere un libro
   post(libro:Libro){
     //http.post prende come parametri (almeno) l'url e i dati da inviare
     return this.http.post(this.url,libro);
   }
}
