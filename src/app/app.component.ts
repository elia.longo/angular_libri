import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LibriService } from './service/libri.service';
import { Libro } from './libro';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  $libri?:Observable<Libro[]>;
  visualizza_add:boolean=false;
  myForm:FormGroup;
  constructor(public libriService:LibriService, public fb:FormBuilder){
    this.$libri=libriService.getAll();
    this.myForm=fb.group({
      id:[''],
      titolo:[''],
      autore:[''],
      prezzoCopertina:['']
    });
  }

  visualizzaAdd(){
    this.visualizza_add=true;
  }

  onSubmit(libro:Libro){
    this.libriService.post(libro).subscribe(res=>{
      console.log(res);
    });
    this.visualizza_add=false;
  }

  annullaAdd(){
    this.visualizza_add=false;
  }
}
